let canvas = document.getElementById("myCanvas");
let ctx = canvas.getContext("2d");
let barHeight = 20;
let barWidth = 120;
let barMarginBottom = 30; 
let bar = {
    x: canvas.width/2 - barWidth/2,
    y: canvas.height - barHeight - barMarginBottom,
    width: barWidth,
    height: barHeight,
    dx: 5
} 
let ballRadius = 10;
let ball = {
    x: canvas.width/2,
    y: bar.y - ballRadius,
    radius: ballRadius,
    speed: 8,
    dx: 2,
    dy: -2 
}
let brick = {
    row: 3,
    column: 9,
    width: 50,
    height: 20,
    offSetLeft: 20,
    offSetTop: 20,
    marginTop: 40,
    fillColor: "#f0ad4e",
    strokeColor: "#292b2c",
    lineWidth: 5
}
let rightAction = false;
let leftAction = false;
let measureAngle = ball.x - (bar.x + bar.width/2);
let collidePoint = measureAngle/(bar.width/2);
let angle = collidePoint * (Math.PI/3);
let life = 3;
let score = 0;
let scoreValue = 100;

document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);

function keyDownHandler(e){
    if(e.key == "Right" || e.key == "ArrowRight"){
        rightAction = true;
    }else if(e.key =="Left" || e.key == "ArrowLeft"){
        leftAction = true;
    }
}

function keyUpHandler(e){
    if(e.key == "Right" || e.key == "ArrowRight"){
        rightAction = false;
    }else if(e.key == "Left" || e.key =="ArrowLeft"){
        leftAction = false;
    }
}

function drawBall(){
        ctx.beginPath();
        ctx.arc(ball.x, ball.y, ball.radius, 0, Math.PI*2, false);
        ctx.fillStyle = "white";
        ctx.fill();
        ctx.strokeStyle ="#292b2c";
        ctx.stroke();
        ctx.closePath();
}

function drawBar(){
    ctx.beginPath();
    ctx.fillStyle = "#f0ad4e";
    ctx.fillRect(bar.x, bar.y, bar.width, bar.height);
    ctx.strokeStyle = "#292b2c";
    ctx.strokeRect(bar.x, bar.y, bar.width, bar.height);
    ctx.lineWidth = 5;
    ctx.closePath();
}

function resetBall(){
    ball.x = canvas.width/2;
    ball.y = bar.y - ball.radius;
    ball.dx = 2*(Math.random()*2-1);
    ball.dy = -2;
}
 
let brickArray = [];
function brickMaker(){
for(let r =0; r<brick.row; r++){

    brickArray[r] = [];

    for(let c=0; c<brick.column; c++)
    {

        brickArray[r][c] = 
        {
            x: c * (brick.offSetLeft + brick.width) +brick.offSetLeft,
            y: r * (brick.offSetTop + brick.height) + brick.offSetTop + brick.marginTop,
            status: true,
        }

    }
}
}

brickMaker();

function drawBricks()
{
    for(let r = 0; r < brick.row; r++)
    {
        for(let c = 0; c < brick.column;c++)
        {
            if(brickArray[r][c].status)
            {
                ctx.fillStyle = brick.fillColor;
                ctx.fillRect(brickArray[r][c].x, brickArray[r][c].y, brick.width, brick.height)
                ctx.strokeColor = brick.strokeColor;
                ctx.strokeRect(brickArray[r][c].x, brickArray[r][c].y, brick.width, brick.height)
            }
        }
    }
}


function ballBrickCollide(){
for(let r = 0; r < brick.row; r++){
    for(let c =0; c < brick.column; c++){
        let b = brickArray[r][c];
        if(b.status){
            if(ball.x + ball.radius > b.x && ball.x - ball.radius < b.x + brick.width && ball.y + ball.radius > b.y
                && ball.y - ball.radius < b.y +brick.height)
                {
                   b.status = false;
                   ball.dy = -ball.dy;
                   score += scoreValue;
                }   
            }
        }
    }
}

function showScore(text, textX, textY){
    ctx.fillStyle = "white";
    ctx.font = "25px Nova Square, cursive";
    ctx.fillText(text, textX, textY);
}


function draw(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawBall();
    drawBar();
    drawBricks();
    ballBrickCollide();
    showScore(score, 30, 30);
    showScore(life, canvas.width - 30, 30);

    if(ball.y + ball.dy < ball.radius){
        ball.dy = -ball.dy;
    }
    else if(ball.y > bar.y && ball.y < bar.y + bar.height && ball.x > bar.x && ball.x < bar.x + bar.width)
    {
            ball.dx = ball.dx || -ball.dx && -ball.dy
            ball.dy = -ball.dy
        }
        else if(ball.y + ball.radius > canvas.height)
            {
                life --
                resetBall();
                }
        
        else if(life == 0){
            alert("GAME OVER! YOU SUCK!");
            document.location.reload();
            clearInterval(interval);
        }
    
    
    if(ball.x + ball.dx < ball.radius || ball.x + ball.dx > canvas.width - ball.radius){
        ball.dx = -ball.dx;
    }

    if(score == (brick.row * brick.column)*100){
        alert("YOU WIN!!! YOU DON'T SUCK!");
        document.location.reload();
        clearInterval(interval);
    }

    
    if(rightAction){
        bar.x += bar.dx;
        if(bar.x + bar.width > canvas.width){
            bar.x = canvas.width - bar.width
        }
    }else if(leftAction){
        bar.x -= bar.dx
        if(bar.x < 0){
            bar.x = 0;
        }
    }
    ball.x += ball.dx;
    ball.y += ball.dy;
}

let interval = setInterval(draw, ball.speed);






